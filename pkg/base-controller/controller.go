/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package basecontroller

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	wait "k8s.io/apimachinery/pkg/util/wait"
	kubeclientset "k8s.io/client-go/kubernetes"
	scheme "k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	cache "k8s.io/client-go/tools/cache"
	record "k8s.io/client-go/tools/record"
	workqueue "k8s.io/client-go/util/workqueue"
)

// Message constants used with events
const (
	ReasonResourceSynced         = "Synced"
	ReasonErrResourceExists      = "ErrResourceExists"
	MessageErrResourceExists     = "Resource %q already exists and is not managed by this controller"
	MessageResourceSyncedCreated = "%q synced successfully (created)"
	MessageResourceSyncedUpdated = "%q synced successfully (updated)"
	MessageResourceSyncedDeleted = "%q synced successfully (deleted)"
)

// IBaseController is the interface of BaseController struct
type IBaseController interface {

	// Public methods
	GetName() string
	Run(int, <-chan struct{}, chan<- error)

	// Abstract methods: Must be implemented in children class.
	// Note1: they must be public, so uppercase is mandatory
	// Note2: if they are not implemented in children class, then a "fatal error:
	// stack overflow" will be occur
	SyncHandler(string) error
	HandleSecondary(obj interface{})

	// "False" public methods: because golang limitations, these methods must be
	// publics because they will be used from the abstract methods implementation
	GetObject(interface{}) (metav1.Object, string, error)
	EnqueueObject(interface{})

	// Private methods
	runWorker()
	processNextWorkItem() bool
	setupHandlers()
}

// BaseController is the base to implement a controller working in the way
// described in:
//   https://github.com/kubernetes/community/blob/master/contributors/devel/sig-api-machinery/controllers.md
//   https://github.com/kubernetes/sample-controller
//
// It means:
// - There is a primary resource, that you'll be updating Status for.
// - There are secondary resources are resources that you'll be creating /
//   updating / deleting
// - All the secondary resources created will have the OwnerRef metadata
//   with the primary resource
//
// The controller using this base should embed it and implement the abtract
// methods (SyncHandler for primary resources and HandleSencondary for
// secondary resources).
type BaseController struct {

	// Property "IBaseController" is required for abstract methods. See:
	// https://medium.com/@adrianwit/abstract-class-reinvented-with-go-4a7326525034
	IBaseController

	// Just a description-name of our controller
	Name string

	// Used for set the OwnerRef.Kind property
	Kind string

	// WorkQueue is used to queue work to be processed instead of performing it as
	// soon as a change happens, thatthat rate limits items being added
	// to the queue.
	Workqueue workqueue.RateLimitingInterface

	// Recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	Recorder record.EventRecorder

	// SyncedFunctions are bool function that can be used to determine if an
	// informer has already synced.
	SyncedFunctions []cache.InformerSynced

	// PrimarySharedInformer is the informer related to the primary resource.
	// A SharedInformer maintains a local cache, that is eventually consistent
	// with the authoritative state. We use its add/delete/modify event notifications
	PrimarySharedInformers []cache.SharedIndexInformer

	// SecondarySharedInformers are the informers related to the secondary resources.
	SecondarySharedInformers []cache.SharedIndexInformer
}

// Run will set up the event handlers for types we are interested in, as well
// as syncing informer caches and starting workers. It will block until stopCh
// is closed, at which point it will shutdown the workqueue and wait for
// workers to finish processing their current work items.
func (c *BaseController) Run(threadiness int, stopCh <-chan struct{}, errCh chan<- error) {
	meth := c.Name + ".Run()"
	log.Info(meth)

	// Many examples of applications include this ...
	//   defer utilruntime.HandleCrash()
	// ... but it isn't a good idea. When an error-panic occur, we want that the
	// controller to die
	defer c.Workqueue.ShutDown()

	// Setup handlers for changes in resources
	c.setupHandlers()

	// Wait for the caches to be synced before starting workers.
	// WaitForCacheSync waits for caches to populate. It returns true if it was
	// successful, false if the controller should shutdown
	log.Infof("%s Waiting for informer caches to sync", meth)
	ok := cache.WaitForCacheSync(stopCh, c.SyncedFunctions...)
	if !ok {
		errCh <- fmt.Errorf("%s failed to wait for caches to sync", meth)
		return
	}

	// Start the worker (maybe several instances, if threadiness>1)
	log.Infof("%s Starting workers", meth)
	for i := 0; i < threadiness; i++ {
		// "wait" is a package providing tools for polling or listening for changes
		// to a condition.
		// wait.Until loops until stop channel is closed, running the provided
		// function every period.
		// Function c.runWorker will loop until "something bad" happens... but
		// ".Until" will then rekick the worker after one second
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	// Wait stopCh is closed
	<-stopCh
	log.Infof("%s Shutting down", meth)
	return
}

// setupHandlers set up the handlers
func (c *BaseController) setupHandlers() {
	meth := c.Name + ".setupHandlers()"
	log.Infof("%s Setting handlers", meth)

	// For primary resources, when a resource is created/updated, just enqueue the element
	for _, sharedInformer := range c.PrimarySharedInformers {
		sharedInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
			AddFunc: c.EnqueueObject,
			UpdateFunc: func(old, new interface{}) {
				// Doubt: should I check if old!=new (because resync updates...)?
				// Shoud I avoid EnqueueObject() if old==new? (for example, because
				// because resync updates...).
				// SampleController example doesn't do that !
				// Maybe the reason is:
				// https://github.com/kubernetes/community/blob/master/contributors/devel/sig-api-machinery/controllers.md
				// "In cases where you are certain that you don't need to requeue items
				// when there are no new changes, you can compare the resource version of
				// the old and new objects. If they are the same, you skip requeuing the
				// work. Be careful when you do this. If you ever skip requeuing your item
				// on failures, you could fail, not requeue, and then never retry that
				// item again."
				c.EnqueueObject(new)
			},
			DeleteFunc: func(obj interface{}) {
				// Do nothing, except log -> this operation is managed by the
				// garbage-collector of Kubernetes (via ownerReference)
				object, _, err := c.GetObject(obj)
				if err != nil {
					log.Warnf("%s.DeleteFunc() key cant be retrieved %v", meth, err)
				} else {
					log.Debugf("%s.DeleteFunc() %s", meth, object.GetName())
				}
			},
		})
	}

	// For secondary resources, work to do depends on controller and resource,
	// and the logic resides in HandleSecondrary (abstract) method.
	// Tipically, HandleSecondary() simply lookup the owner and enqueue the owner
	// to be processed, so we don't need to implement custom logic for
	// handling secondary resources. More info on this pattern:
	// https://github.com/kubernetes/community/blob/master/contributors/devel/sig-api-machinery/controllers.md
	// "Many controllers must trigger off multiple resources (I need to "check X
	// if Y changes"), but nearly all controllers can collapse those into a queue
	// of “check this X” based on relationships. For instance, a ReplicaSet
	// controller needs to react to a pod being deleted, but it does that by
	// finding the related ReplicaSets and queuing those.

	for _, sharedInformer := range c.SecondarySharedInformers {
		sharedInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
			AddFunc: c.HandleSecondary,
			UpdateFunc: func(old, new interface{}) {
				newScr := new.(metav1.Object)
				oldScr := old.(metav1.Object)
				// Periodic resync will send update events... in this case (when
				// resource version has not changed) is not necessary to check anything
				if newScr.GetResourceVersion() != oldScr.GetResourceVersion() {
					c.HandleSecondary(new)
				}
			},
			DeleteFunc: c.HandleSecondary,
		})
	}
}

// EnqueueObject takes a primary resource and converts it into a namespace/name
// string (the key) which is then put onto the work queue
func (c *BaseController) EnqueueObject(obj interface{}) {
	meth := c.Name + ".EnqueueObject()"
	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		log.Warnf("%s %v", meth, err)
		return
	}
	log.Debugf("%s key = %s", meth, key)
	c.Workqueue.Add(key)
}

// GetObject returns, using a generic interface{} reference, the secondary
// resource and its owner name.
func (c *BaseController) GetObject(objSrc interface{}) (objDst metav1.Object, ownerRefName string, err error) {
	meth := c.Name + ".GetObject()"

	var ok bool
	objDst, ok = objSrc.(metav1.Object)
	if !ok {
		// About "DeletedFinalStateUknown"
		// Comment in client-go/delta-fifo.go/DeletedFinalStateUknown struct.
		// "DeletedFinalStateUnknown is placed into a DeltaFIFO in the case where
		// an object was deleted but the watch deletion event was missed. In this
		// case we don't know the final "resting" state of the object, so there's
		// a chance the included `Obj` is stale."
		// Comment in client-go/controller/onResourceHandler/onDelete
		// "OnDelete will get the final state of the item if it is known, otherwise
		//  happen if the watch is closed and misses the delete event and we don't
		//  it will get an object of type DeletedFinalStateUnknown. This can
		//  notice the deletion until the subsequent re-list.
		tombstone, ok := objSrc.(cache.DeletedFinalStateUnknown)
		if !ok {
			objDst = nil
			err = fmt.Errorf("%s Error decoding object, invalid type", meth)
			return
		}
		objDst, ok = tombstone.Obj.(metav1.Object)
		if !ok {
			objDst = nil
			err = fmt.Errorf("%s Error decoding object tombstone, invalid type", meth)
			return
		}
		log.Infof("%s Recovered deleted object '%s' from tombstone", meth, objDst.GetName())
	}

	ownerRef := metav1.GetControllerOf(objDst)
	if (ownerRef != nil) && (ownerRef.Kind == c.Kind) {
		ownerRefName = ownerRef.Name
	}

	return
}

// runWorker is a long-running function that will continually call the
// processNextWorkItem function in order to read and process a message on the
// workqueue.
func (c *BaseController) runWorker() {
	for c.processNextWorkItem() {
	}
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
// It returns false when it's time to quit (shutdown).
//
//                    |
//                    |new key
//                    |
//         +----------|----------+
//         |                     |   key
//    +-----  workqueue.get()    -<--------------------+
//    |    |                     |                     |
//    |    +----------|----------+                     |
//    |               |key                             |
//    |               |                                |
//    |    +----------|----------+          +----------|----------+
//    |    |                     |          |                     |
//    |    |  valid item?        |          |  workqueue.add()    |
//    |    |                     |          |  [with ratelimite]  |
//    |    +----------|----------+          |                     |
//    |               |yes                  +----------^----------+
//    |no             |                                |
//    |    +----------|----------+                     |
//    |    |                     |                     |
//    |    |  syncHandler()      |   error             |
//    |    |  [conciliate]       ----------------------+
//    |    |                     |
//    |    +----------|----------+
//    |               |
//    |               | ok
//    |               |
//    |    +----------|----------+
//    |    |                     |
//    +--->|  workqueue.forget() |
//         |                     |
//         +----------|----------+
//                    |
//                    |
//         +----------|----------+
//         |                     |
//         |  workqueue.done()   |
//         |                     |
//         +---------------------+
//
func (c *BaseController) processNextWorkItem() bool {
	meth := c.Name + ".processNextWorkItem()"

	// Pull the next work item from queue. It should be a key we use to lookup
	// something in a cache
	obj, shutdown := c.Workqueue.Get()
	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.  ¿? review
	err := func(obj interface{}) error {
		// You always have to indicate to the queue that you've completed a piece of
		// work.
		// We also must remember to call Forget if we do not want this work item
		// being re-queued. For example, we do not call Forget if a transient error
		// occurs, instead the item is put back on the workqueue and attempted again
		// after a back-offperiod.
		defer c.Workqueue.Done(obj)

		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		var key string
		var ok bool
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.Workqueue.Forget(obj)
			log.Warnf("%s expected string in Workqueue but got %v", meth, obj)
			return nil
		}

		// Run the syncHandler, passing it the namespace/name string of the object
		// to be synced.
		log.Debugf("%s Calling SyncHandler()", meth)
		if err := c.SyncHandler(key); err != nil {
			// Put the item back on the workqueue to handle any transient errors.
			// Since we failed, we should requeue the item to work on later.
			// AddRateLimited adds an item to the workqueue after the rate limiter
			// says it's ok.
			// This method will add a backoff to avoid hotlooping on particular items
			// (they're probably still not going to work right away) and overall
			// controller protection (everything I've done is broken, this controller
			// needs to calm down or it can starve other useful work) cases.
			log.Warnf("%s error syncing %v", meth, err)
			c.Workqueue.AddRateLimited(key)
			return fmt.Errorf("error syncing '%s': %v, requeuing", key, err)
		}

		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.Workqueue.Forget(obj)
		return nil
	}(obj)

	if err != nil {
		log.Warnf("%s error %v", meth, err)
		return true
	}
	return true
}

// Example of HandleSecondary function, that is in charge of processing changes
// in the secondary objects (kube-secrets in this case) and force to re-evaluate
// the primary object (kuku-cert in this case)
//
// func (c *KukuCertController) HandleSecondary(obj interface{}) {
// 	meth := c.Name + ".HandleSecondary()"
// 	log.Info(meth)
// 	kubeSecret, kukuCertName, err := c.GetObject(obj)
// 	if err != nil {
// 		log.Errorf("%s Error %v", meth, err)
// 		return
// 	}
// 	if kukuCertName != "" {
// 		log.Debugf("%s Processing object: %s", meth, kubeSecret.GetName())
// 		kukucert, err := c.kukuCertLister.KukuCerts(kubeSecret.GetNamespace()).Get(kukuCertName)
// 		if errors.IsNotFound(err) {
// 			log.Infof("%s ignoring orphaned object '%s' of kukucert '%s'", meth, kubeSecret.GetSelfLink(), kukuCertName)
// 			return
// 		} else if err != nil {
// 			log.Warnf("%s error retrieving object %s: %v", meth, kukuCertName, err)
// 		}
// 		log.Debugf("%s enqueue %s", meth, kukucert.Name)
// 		c.EnqueueObject(kukucert)
// 	}
// }

// Example of SyncHandler function, processing the primary resource (kuku-cert
// in this case), and that is in charge of compares the actual state with the
// desired, and attempts to converge the two.
//
// func (c *KukuCertController) SyncHandler(key string) error {
// 	meth := c.Name + ".SyncHandler() "
// 	log.Infof("%s %s", meth, key)
//
// 	namespace, name, err := cache.SplitMetaNamespaceKey(key)
// 	if err != nil {
// 		log.Errorf("%s invalid resource key: %s", meth, key)
// 		return nil
// 	}
//
// 	kukucert, err := c.kukuCertLister.KukuCerts(namespace).Get(name)
// 	if err != nil {
// 		if errors.IsNotFound(err) {
// 			log.Warnf("%s '%s' in work queue no longer exists", meth, key)
// 			return nil
// 		}
// 		log.Errorf("%s Lister: %v", meth, err)
// 		return err
// 	}
//
// 	secretName := naming.CalculateSecretName(name)
// 	secret, err := c.secretLister.Secrets(namespace).Get(secretName)
// 	secretCreated := false
// 	if errors.IsNotFound(err) {
// 		log.Infof("%s secret %s must be created", meth, secretName)
// 		secret, err = c.kubeClientset.CoreV1().Secrets(namespace).Create(newSecret(kukucert, namespace))
// 		if err == nil {
// 			secretCreated = true
// 		} else {
// 			log.Errorf("%s Creating secret: %v", meth, err)
// 			return err
// 		}
// 	} else if err != nil {
// 		log.Errorf("%s SecretLister: %v", meth, err)
// 		return err
// 	}
// 	if !metav1.IsControlledBy(secret, kukucert) {
// 		msg := fmt.Sprintf(base.MessageErrResourceExists, secret.Name)
// 		c.Recorder.Event(kukucert, corev1.EventTypeWarning, base.ReasonErrResourceExists, msg)
// 		return fmt.Errorf(msg)
// 	}
//
// 	secretUpdated := false
// 	if !secretCreated {
// 		if !isEqualSecret(kukucert, secret) {
// 			log.Infof("%s secret %s must be updated", meth, secretName)
// 			secret, err = c.kubeClientset.CoreV1().Secrets(namespace).Update(newSecret(kukucert, namespace))
// 			if err == nil {
// 				secretUpdated = true
// 			} else {
// 				log.Errorf("%s Updating secret: %v", meth, err)
// 				return err
// 			}
// 		}
// 	}
//
// 	if secretUpdated {
// 		msg := fmt.Sprintf(base.MessageResourceSyncedUpdated, "Secret")
// 		c.Recorder.Event(secret, corev1.EventTypeNormal, base.ReasonResourceSynced, msg)
// 		log.Infof("%s Secret updated", meth)
// 	} else if secretCreated {
// 		msg := fmt.Sprintf(base.MessageResourceSyncedCreated, "Secret")
// 		c.Recorder.Event(secret, corev1.EventTypeNormal, base.ReasonResourceSynced, msg)
// 		log.Infof("%s Secret created", meth)
// 	} else {
// 		log.Infof("%s Nothing to do", meth)
// 	}
//
// 	return nil
// }

//
// Utilities
//

// CreateRecorder creates an event-broadcaster and and event-recorder, and
// returns the event-recorder.
// Add kumori types to the default Kubernetes Scheme so Events can be logged
// for kumori types.
// Example of use:
//   import (
//   	 kubeclientset "k8s.io/client-go/kubernetes"
//     myscheme "my-controller/pkg/generated/clientset/versioned/scheme"
//   )
//   controllerName = "mycontroller"
//   recorder = base.CreateRecorder(kubeClientset, myscheme.AddToScheme, controllerName),
func CreateRecorder(
	kubeClientset kubeclientset.Interface,
	addToScheme func(s *runtime.Scheme) error,
	controllerName string) record.EventRecorder {
	utilruntime.Must(addToScheme(scheme.Scheme)) // "Must": panic if error
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(log.Infof)
	eventBroadcaster.StartRecordingToSink(
		&typedcorev1.EventSinkImpl{Interface: kubeClientset.CoreV1().Events("")},
	)
	recorder := eventBroadcaster.NewRecorder(
		scheme.Scheme,
		corev1.EventSource{Component: controllerName},
	)
	return recorder
}
