/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

// Package stopsignal Uses a channel to inform to different components of an
// application that a signal (SIGINT, SIGTERM) has been detected.
package stopsignal

import (
	"os"
	"os/signal"
	"syscall"
)

// Interesting signals: SIGINT (ctrl-c) and SIGTERM
var shutdownSignals = []os.Signal{os.Interrupt, syscall.SIGTERM}

// Dirty trick (used in sample-controller/signal.go): use a void channel to
// detect if a function is invoked twice
var onlyOnceAllowed = make(chan struct{})

// SetupStopSignal returns a receive-only void channel ("<-chan struct{}""), wich
// is closed when SIGTERM or SIGINT signal is caught.
// If a second signal is caught, the program is terminated with exit code 1.
func SetupStopSignal() (stopCh <-chan struct{}) {
	close(onlyOnceAllowed) // panics when called twice!
	stop := make(chan struct{})
	c := make(chan os.Signal, 2)
	signal.Notify(c, shutdownSignals...)
	go func() {
		<-c
		close(stop)
		<-c
		os.Exit(1) // second signal. Exit directly.
	}()
	return stop
}
